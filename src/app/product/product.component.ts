import { Product } from './../product.model';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['.././hello.component.css']
})
export class ProductComponent implements OnInit {
  @Input() product: Product [];
  @Output() onRemoveProduct = new EventEmitter();
  @Output() onUpdateQuatity = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  removeProduct(param:string):void {
    alert('Định xóa à?' + param);
    this.onRemoveProduct.emit(param);

  }

  updateQuatity(id:string, element:HTMLInputElement){
    alert("Định thay đổi thành bao nhiêu? " + element.value)
    this.onUpdateQuatity.emit({id, element});
  }

}
