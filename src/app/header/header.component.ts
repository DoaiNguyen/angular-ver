import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['.././hello.component.css']
})
export class HeaderComponent implements OnInit {
  title = "Shoping Cart";
  @Input() numberItem:number;
  constructor() { }

  ngOnInit(): void {
  }

}
