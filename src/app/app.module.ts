import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HelloConponent } from './hello.component';
import { HeaderComponent } from './header/header.component';
import { ProductComponent } from './product/product.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TableComponent } from './table/table.component';
@NgModule({
  declarations: [
    AppComponent,
    HelloConponent,
    HeaderComponent,
    ProductComponent,
    DashboardComponent,
    TableComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
