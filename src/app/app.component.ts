import { Component, OnInit } from '@angular/core';
import { Product } from './product.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'angular-ver';
  numberItem : number = 0;
  product : Product [] = [
    {
      id: '1',
      name: 'PRODUCT ITEM NUMBER 3',
      description: 'Description for product item number 1',
      image: 'https://via.placeholder.com/200x150',
      price: 5.995,
      quatity: 28
    },
    {
      id: '2',
      name: 'PRODUCT ITEM NUMBER 2',
      description: 'Description for product item number 2',
      image: 'https://via.placeholder.com/200x150',
      price: 19.99,
      quatity: 12
    }
  ];

  ngOnInit():void{
    //chay khi bat dau load trang
    //tinh lai tong so luong
    let numberItem = 0;
    for(const pro of this.product){
      numberItem += pro.quatity;
    }
    this.numberItem = numberItem;
  }


  removeProduct(param:string){
    alert("Được của nó" + param);

    //xoa san pham
    const index = this.product.findIndex(pro => pro.id == param);
    this.product.splice(index, 1);

    //tinh lai tong so luong
    let numberItem = 0;
    for(const pro of this.product){
      numberItem += pro.quatity;
    }
    this.numberItem = numberItem;
  }

  updateQuatity(p: {id:string, element:HTMLInputElement}){
    alert("update số lượng của nó" + p.element.value);
    const pro = this.product.find(pro => pro.id = p.id);
    if(pro){
      pro.quatity = parseInt(p.element.value);
    }

    //tinh lai tong so luong
    let numberItem = 0;
    for(const pro of this.product){
      numberItem += pro.quatity;
    }
    this.numberItem = numberItem;
  }
}
